#ifndef __CONFIGS_H__
#define __CONFIGS_H__

/**
 @brief Developer information of Admob
 */

#define SET_LEFT_VALUE(v)    UserDefault::getInstance()->setIntegerForKey("leftValue", v);
#define GET_LEFT_VALUE       UserDefault::getInstance()->getIntegerForKey("leftValue");

#define SET_RIGHT_VALUE(v)   UserDefault::getInstance()->setIntegerForKey("rightValue", v);
#define GET_RIGHT_VALUE      UserDefault::getInstance()->getIntegerForKey("rightValue");

#define SET_AD_VALUE(v)   UserDefault::getInstance()->setIntegerForKey("AdCountValue", v)
#define GET_AD_VALUE      UserDefault::getInstance()->getIntegerForKey("AdCountValue")

#define VALUE_FLUSH          UserDefault::getInstance()->flush();

#define IS_ENGLISH  Application::getInstance()->getCurrentLanguage() == LanguageType::ENGLISH

#define AD_COUNT 5

#define INFO_HTML "htmls/Banner/banners_big.html"

#define CHINA_ZODIAC false //true

//zodiac circle force
#define FORCE_TOUCH 9000.0f

//disable facebook
#define ENABLE_FB true

#define BANNER_URL1 "http://www.baidu.com"
#define BANNER_URL2 "http://www.baidu.com"
#define BANNER_URL3 "http://www.baidu.com"
#define BANNER_URL4 "http://www.baidu.com"
#define BANNER_URL5 "http://www.baidu.com"
#define BANNER_URL6 "http://www.baidu.com"
#define BANNER_URL7 "http://www.baidu.com"
#define BANNER_URL8 "http://www.baidu.com"
#define BANNER_URL9 "http://www.baidu.com"
#define BANNER_URL10 "http://www.baidu.com"
#define BANNER_URL11 "http://www.baidu.com"
#define BANNER_URL12 "http://www.baidu.com"

#endif // __CONFIGS_H__
