#include "InfoScene.h"
#include "Configs.h"
USING_NS_CC;



Scene* InfoScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
	auto layer = InfoScene::create();
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool InfoScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Sprite *bg = Sprite::create("BG/BG_InfoScreen.png");
    bg->setPosition(visibleSize/2.0);
    this->addChild(bg);
    
	auto backItem = MenuItemImage::create("Buttons/Btn_Back.png",
                                           "Buttons/Btn_Back_Pressed.png",
                                           CC_CALLBACK_1(InfoScene::menuCallback, this));

	backItem->setPosition(Vec2(visibleSize.width * 0.1, visibleSize.height * 0.2));

    auto menu = Menu::create(backItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)   
	webView = WebView::create();
	webView->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height / 2.0 - 12 ));
	webView->setContentSize(Size(visibleSize.width * 0.7, visibleSize.height * 0.7));
    webView->setScalesPageToFit(false);
	webView->loadFile(INFO_HTML);
	this->addChild(webView);
#endif
    
    auto listener = EventListenerKeyboard::create();
	listener->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event){
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK ||
            keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE)
        {
            Director::getInstance()->popScene();
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void InfoScene::menuCallback(Ref* pSender)
{
	Director::getInstance()->popScene();
}
