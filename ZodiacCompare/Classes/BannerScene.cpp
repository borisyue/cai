#include "BannerScene.h"
#include "SectionScene.h"
#include "DisplayTScene.h"
#include "ModalLayer.h"
#include "ui/CocosGUI.h"
#include "Configs.h"
USING_NS_CC;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
using namespace cocos2d::experimental::ui;
#endif

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
//#include "src/Direct3DInterop.h"
//using namespace PhoneDirect3DXamlAppComponent;
//#endif

Scene* BannerScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = BannerScene::create();
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool BannerScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();

    Sprite *bg = Sprite::create("BG/BG_GMGScreen.png");
    bg->setPosition(visibleSize/2.0);
    this->addChild(bg);
    
    Sprite *title = Sprite::create(IS_ENGLISH?"Components/ZodiacTitle_GMG_EN.png":"Components/ZodiacTitle_GMG_DE.png");
    title->setPosition(Vec2(visibleSize.width * 0.82, visibleSize.height * 0.73));
    this->addChild(title);
    
    auto closeItem = MenuItemImage::create(
                                           "Buttons/Btn_OK.png",
                                           "Buttons/Btn_OK_Pressed.png",
                                           CC_CALLBACK_1(BannerScene::menuCallback, this));
    
	closeItem->setPosition(Vec2(visibleSize.width * 0.9, visibleSize.height * 0.15));
	  
    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
	dataAry.push_back("htmls/Banner/animalpuzzle_big.png");
	dataAry.push_back("htmls/Banner/beat_big.png");
	dataAry.push_back("htmls/Banner/devil_big.png");
	dataAry.push_back("htmls/Banner/dice_big.png");
	dataAry.push_back("htmls/Banner/dirt_big.png");
	dataAry.push_back("htmls/Banner/halloween_big.png");
	dataAry.push_back("htmls/Banner/holdemcalculator_big.png");
	dataAry.push_back("htmls/Banner/oldmacnoise_big.png");
	dataAry.push_back("htmls/Banner/rocket_big.png");
	dataAry.push_back("htmls/Banner/sazzles_big.png");
	dataAry.push_back("htmls/Banner/sushi_big.png");
	dataAry.push_back("htmls/Banner/zodiac_big.png");

	TableView  *tableView = TableView::create(this, CCSizeMake(visibleSize.width * 0.63, visibleSize.height));
	tableView->setDirection(TableView::Direction::VERTICAL);
	tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	tableView->ignoreAnchorPointForPosition(false);
	tableView->setAnchorPoint(Vec2::ZERO);
	tableView->setPosition(Vec2::ZERO);
	tableView->setDelegate(this);
	tableView->setBounceable(false);
	this->addChild(tableView, 2);
	tableView->reloadData();
#endif

	auto listener_keyboard = EventListenerKeyboard::create();
	listener_keyboard->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event){
		if (keyCode == EventKeyboard::KeyCode::KEY_BACK ||
			keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE)
		{
			if (Director::getInstance()->getRunningScene()->getTag() == 16) {
				Director::getInstance()->popScene();
			}
			else{
				Director::getInstance()->end();
				exit(0);
			}
		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener_keyboard, this);

    return true;
}

void BannerScene::onEnter()
{
    Layer::onEnter();
	Size visibleSize = Director::getInstance()->getVisibleSize();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    webView = WebView::create();
    webView->setPosition(Vec2(visibleSize.width * 0.33, visibleSize.height / 2.0));
    webView->setContentSize(Size(visibleSize.width * 0.63 , visibleSize.height));
    webView->loadFile("htmls/Banner/banners_big.html");
    webView->setScalesPageToFit(true);
    this->addChild(webView);
#endif

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
//	PhoneDirect3DXamlAppComponent::BroswerEventHelper^ helper = ref new BroswerEventHelper();
//	helper->ShowWebBroswer(false,0,0, visibleSize.width * 0.57, visibleSize.height,"htmls/Banner/banners_big.html");
//#endif
}

void BannerScene::onExit()
{
    Layer::onExit();
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    webView->removeFromParentAndCleanup(true);
#endif
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
//	PhoneDirect3DXamlAppComponent::BroswerEventHelper^ helper = ref new BroswerEventHelper();
//	helper->ShowWebBroswer(true, 0, 0, 0, 0, "htmls/Banner/banners_big.html");
//#endif
}

void BannerScene::menuCallback(Ref* pSender)
{
    if (Director::getInstance()->getRunningScene()->getTag() == 16) {
        Director::getInstance()->replaceScene(DisplayTScene::createScene());
    }else{
        Director::getInstance()->pushScene(SectionScene::createScene());
    }
}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
void BannerScene::tableCellTouched(TableView* table, TableViewCell* cell)
{
	CCLOG("cell touched at index: %i", cell->getIdx());
	std::string targetURL = BANNER_URL1;
	switch (cell->getIdx())
	{
	case 0:
		targetURL = BANNER_URL1;
		break;
	case 1:
		targetURL = BANNER_URL2;
		break;
	case 2:
		targetURL = BANNER_URL3;
		break;
	case 3:
		targetURL = BANNER_URL4;
		break;
	case 4:
		targetURL = BANNER_URL5;
		break;
	case 5:
		targetURL = BANNER_URL6;
		break;
	case 6:
		targetURL = BANNER_URL7;
		break;
	case 7:
		targetURL = BANNER_URL8;
		break;
	case 8:
		targetURL = BANNER_URL9;
		break;
	case 9:
		targetURL = BANNER_URL10;
		break;
	case 10:
		targetURL = BANNER_URL11;
		break;
	case 11:
		targetURL = BANNER_URL12;
		break;
	default:
		break;
	}
	Application::getInstance()->openURL(targetURL);
}

Size BannerScene::tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
	return Size(table->getContentSize().width, 200);
}

TableViewCell* BannerScene::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
	TableViewCell *cell = new TableViewCell();
	cell->autorelease();

	std::string str = dataAry.at(idx);
	Sprite *spr = CCSprite::create(str);
	spr->setPosition(ccp(table->getContentSize().width / 2, 200 / 2));
	cell->addChild(spr);

	return cell;
}

ssize_t BannerScene::numberOfCellsInTableView(cocos2d::extension::TableView *table)
{
	return dataAry.size();
}
#endif
