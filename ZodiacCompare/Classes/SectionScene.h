#ifndef __SECTION_SCENE_H__
#define __SECTION_SCENE_H__

#include "cocos2d.h"
#include "ModalLayer.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "ProtocolAds.h"
#endif
USING_NS_CC;

typedef enum
{
    kRotateing = 0,
    kRotateStop,
} RotateStatus;

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
class SectionScene : public cocos2d::Layer, public cocos2d::plugin::AdsListener
#else
class SectionScene : public cocos2d::Layer
#endif
{
private:
    RotateStatus rs;
    int c_rotateLeft,c_rotateRight;
    int year,month,day, year2, month2, day2;
    bool isInit, dpLeft;
    cocos2d::PhysicsBody *bodyLeft,*bodyRight;
    cocos2d::Sprite *currentSpriteLeft,*currentSpriteRight, *SpriteLeft, *SpriteRight;
    cocos2d::Label  *year_label, *month_label, *day_label, *year_label2, *month_label2, *day_label2;
    ModalLayer *modal;
protected:
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    cocos2d::plugin::ProtocolAds* _admob;
    cocos2d::plugin::TAdsInfo adInfo;
    virtual void onAdsResult(cocos2d::plugin::AdsResultCode code, const char* msg);
#endif    
    Scene *v_scene;
    std::unordered_map<int, Node*> _mouses;
    
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual void onEnter();
    virtual void onExit();

    // a selector callback
    void checkEuropeZodiac();
    void checkChinaZodiac();
    void createPicker();
    void pickerCallback(cocos2d::Ref *pSender);
    void menuCallback(cocos2d::Ref* pSender);
	void scheduleCallback(float dt);
	void loadingAnimating();
    void createCircleTurn();
    void updateCurrentSprite(int left, int right);
    void autoFitCorner(bool isLeft, int corner);
    void buffering();
    bool onTouchBegan(Touch* touch, Event* event);
    void onTouchMoved(Touch* touch, Event* event);
    void onTouchEnded(Touch* touch, Event* event);
    // implement the "static create()" method manually
	CREATE_FUNC(SectionScene);
};

#endif // __HELLOWORLD_SCENE_H__
