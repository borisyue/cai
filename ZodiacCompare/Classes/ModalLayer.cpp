#include "ModalLayer.h"

// on "init" you need to initialize your instance
bool ModalLayer::init()
{
    //////////////////////////////
    // 1. super init first
//    if ( !LayerColor::initWithColor(Color4B::BLACK))
    if ( !LayerColor::init())
    {
        return false;
    }
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = [](Touch *touch, Event *event)
    {
        return true;
    };
    listener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void ModalLayer::addLoading(const std::string& loadingimg)
{
    Vec2 visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite *spr = Sprite::create(loadingimg);
    spr->setPosition(visibleSize/2.0);
    this->addChild(spr);
    spr->runAction(RepeatForever::create(RotateBy::create(1.0, 60)));
    
    this->setColor(Color3B::BLACK);
    this->setOpacity(255.0f);
}

void ModalLayer::addQuitConfirm(const std::string& img1, const std::string& selectedimg1, const ccMenuCallback& callback1,
                                const std::string& img2, const std::string& selectedimg2, const ccMenuCallback& callback2,
                                const std::string& bgimg)
{
    Vec2 visibleSize = Director::getInstance()->getVisibleSize();
    
    Sprite *bg = Sprite::create(bgimg);
    bg->setPosition(visibleSize/2.0);
    this->addChild(bg);
    
    auto item1 = MenuItemImage::create(img1, selectedimg1, callback1);
    auto item2 = MenuItemImage::create(img2, selectedimg2, callback2);
    auto menu = Menu::create(item1, item2, NULL);
    menu->alignItemsHorizontallyWithPadding(bg->getContentSize().width / 2.0);
    menu->setPosition(Vec2(bg->getContentSize().width / 2.0, item1->getContentSize().height / 2.0 + 30));
    bg->addChild(menu, 1);
}
