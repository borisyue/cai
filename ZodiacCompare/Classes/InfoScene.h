#ifndef __INFO_SCENE_H__
#define __INFO_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
using namespace cocos2d::experimental::ui;
#endif
USING_NS_CC;

class InfoScene : public cocos2d::Layer
{
private:
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    WebView *webView;
#endif
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    // a selector callback
    void menuCallback(cocos2d::Ref* pSender);

    CREATE_FUNC(InfoScene);
};

#endif // __HELLOWORLD_SCENE_H__
