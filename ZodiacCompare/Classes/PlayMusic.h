//
//  PlayMusic.h
//  PuzzleGame
//
//  Created by Volker Eloesser on 15/07/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _PLAYMUSIC_H_
#define _PLAYMUSIC_H_

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
//usage
//extern bool stopMusic;
//extern bool stopSound;
//void MainMenuLayer::SoundClick(CCMenuItem *sender)
//{
//    stopSound=!stopSound;
//    PlayMusic::PlayShortMusic("buttonExceptConfirm.mp3");
//}
//
//void MainMenuLayer::MusicClick(CCMenuItem *sender)
//{
//    stopMusic =! stopMusic;
//    if (stopMusic) {
//        PlayMusic::pauseMusic();
//    }
//    else {
//        PlayMusic::playMusic();
//    }
//    PlayMusic::PlayShortMusic("buttonExceptConfirm.mp3");
//}
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
class CC_DLL PlayMusic
#else
class PlayMusic
#endif
{
public:
    static void playWithFileName(const char* fileName);
    static void playMusic();
    static void checkMusic();
    static void stopMusicFunction();
    static void pauseMusic();
    static void getUserDefaults();
    static void saveUserDefaults();
    static void closeMusic();
    static void PlayShortMusic(const char* fileName);
};

#endif
