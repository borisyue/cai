//
//  PlayMusic.m
//  PuzzleGame
//
//  Created by Volker Eloesser on 15/07/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "PlayMusic.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
using namespace CocosDenshion;

bool stopMusic;
bool stopSound;

void PlayMusic::playWithFileName(const char* fileName)
{	
	//std::string(CCFileUtils::fullPathFromRelativePath(EFFECT_FILE)).c_str()
    if (!stopMusic) {
		SimpleAudioEngine::getInstance()->playBackgroundMusic(std::string(cocos2d::CCFileUtils::getInstance()->fullPathForFilename(fileName)).c_str(),true);
	}    
}

void PlayMusic::playMusic()
{
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	stopMusic=false;
}

void PlayMusic::checkMusic()
{
    if (stopMusic) {
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
    else
    {
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    }
}

void PlayMusic::pauseMusic()
{
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	stopMusic=true;
}

void PlayMusic::closeMusic(){
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void PlayMusic::stopMusicFunction(){
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void PlayMusic::getUserDefaults(){
	stopMusic = cocos2d::CCUserDefault::getInstance()->getBoolForKey("Music");
    stopSound = cocos2d::CCUserDefault::getInstance()->getBoolForKey("Sound");
}

void PlayMusic::saveUserDefaults(){
	cocos2d::CCUserDefault::getInstance()->setBoolForKey("Music",stopMusic);
	cocos2d::CCUserDefault::getInstance()->setBoolForKey("Sound",stopSound);
}

void PlayMusic::PlayShortMusic(const char* fileName)
{
    if (!stopSound){
        //[[SimpleAudioEngine sharedEngine] playEffect:fileStr];
        cocos2d::log("play Sound:%s",fileName);
		SimpleAudioEngine::getInstance()->playEffect(std::string(cocos2d::CCFileUtils::getInstance()->fullPathForFilename(fileName)).c_str());
    }
}
