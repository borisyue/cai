#ifndef __DISPLAYT_SCENE_H__
#define __DISPLAYT_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "ProtocolAds.h"
using namespace cocos2d::experimental::ui;
#endif
USING_NS_CC;

class DisplayTScene : public cocos2d::Layer
{
private:
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    cocos2d::plugin::ProtocolAds* _admob;
    cocos2d::plugin::TAdsInfo adInfo;
    WebView *webView;
#endif
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual void onEnter();
    virtual void onExit();
    std::string getHTMLFileName();
    // a selector callback
    void menuCallback(cocos2d::Ref* pSender);
    void shareToFB(cocos2d::Ref* pSender);
    // implement the "static create()" method manually
    CREATE_FUNC(DisplayTScene);
};

#endif // __HELLOWORLD_SCENE_H__
