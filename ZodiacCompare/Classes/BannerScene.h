#ifndef __BANNER_SCENE_H__
#define __BANNER_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;

class BannerScene : public cocos2d::Layer
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
#endif
{
private:
#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
	std::vector<std::string> dataAry;
	//cocos2d::Vector<cocos2d::String> dataAry;
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    cocos2d::experimental::ui::WebView *webView;
#endif
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    virtual void onEnter();
    virtual void onExit();
    // a selector callback
    void menuCallback(cocos2d::Ref* pSender);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view){};
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {};
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
#endif
    CREATE_FUNC(BannerScene);
};

#endif // __HELLOWORLD_SCENE_H__
