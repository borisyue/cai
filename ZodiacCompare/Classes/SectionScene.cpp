#include "SectionScene.h"
#include "DisplayTScene.h"
#include "ui/CocosGUI.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "PluginManager.h"
#include "FacebookAgent.h"
using namespace cocos2d::plugin;
#endif
#include "Configs.h"
#include "PlayMusic.h"
#include "BannerScene.h"
#include "ModalLayer.h"
#include "InfoScene.h"
USING_NS_CC;

extern bool stopSound;

int daysWithYearMonth(int v_year, int v_month);
int ChinaZodiacFromDate(int year, int month, int day);
int EuropeZodiacFromDate(int month, int day);
Scene* SectionScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    // 'layer' is an autorelease object
	auto layer = SectionScene::create();
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SectionScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    Sprite *bg = Sprite::create("BG/BG_SelectionScreen.png");
    bg->setPosition(visibleSize/2.0);
    this->addChild(bg);
    
    Sprite *title = Sprite::create(IS_ENGLISH?"Components/Title_EN.png":"Components/Title_DE.png");
    title->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height * 0.92));
    this->addChild(title);
    
    auto infoItem = MenuItemImage::create("Buttons/Btn_Info.png",
                                          "Buttons/Btn_Info_Pressed.png",
                                          CC_CALLBACK_1(SectionScene::menuCallback, this));
    infoItem->setTag(1);
	infoItem->setPosition(Vec2(visibleSize.width * 0.1, visibleSize.height * 0.88));

    auto gmgItem = MenuItemImage::create("Buttons/Btn_GMG.png",
                                          "Buttons/Btn_GMG_Pressed.png",
                                          CC_CALLBACK_1(SectionScene::menuCallback, this));
    gmgItem->setTag(2);
    gmgItem->setPosition(Vec2(visibleSize.width * 0.9, visibleSize.height * 0.85));
    
    auto fbItem = MenuItemImage::create("Buttons/Btn_FB.png",
                                          "Buttons/Btn_FB_Pressed.png",
                                          CC_CALLBACK_1(SectionScene::menuCallback, this));
    fbItem->setTag(3);
    fbItem->setPosition(Vec2(visibleSize.width * 0.9, visibleSize.height * 0.95));
    
    auto sndItem = MenuItemToggle::createWithCallback(CC_CALLBACK_1(SectionScene::menuCallback, this),
                                                      MenuItemImage::create("Buttons/Btn_SNDON.png","Buttons/Btn_SNDON.png"),
                                                      MenuItemImage::create("Buttons/Btn_SNDOFF.png","Buttons/Btn_SNDOFF.png"),
                                                      NULL
                                                      );
    sndItem->setTag(4);
    sndItem->setPosition(Vec2(visibleSize.width * 0.9, visibleSize.height * 0.75));
    sndItem->setSelectedIndex(stopSound?1:0);

    auto leftdpItem = MenuItemImage::create("datepicker/Date.png",
                                        "datepicker/Date_press.png",
                                        CC_CALLBACK_1(SectionScene::menuCallback, this));
    leftdpItem->setTag(6);
    leftdpItem->setPosition(Vec2(visibleSize.width / 2.0 - 200, visibleSize.height * 0.16));
    
    auto rightdpItem = MenuItemImage::create("datepicker/Date.png",
                                        "datepicker/Date_press.png",
                                        CC_CALLBACK_1(SectionScene::menuCallback, this));
    rightdpItem->setTag(7);
    rightdpItem->setPosition(Vec2(visibleSize.width / 2.0 + 210, visibleSize.height * 0.79));
    
    auto okItem = MenuItemImage::create("Buttons/Btn_OK.png",
                                          "Buttons/Btn_OK_Pressed.png",
                                          CC_CALLBACK_1(SectionScene::menuCallback, this));
    okItem->setTag(5);
    okItem->setPosition(Vec2(visibleSize.width * 0.9, visibleSize.height * 0.08));

    auto menu = Menu::create(infoItem, gmgItem, sndItem, fbItem, okItem, leftdpItem, rightdpItem, NULL);
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    FacebookAgent::FBInfo params;
    params.insert(std::make_pair("dialog", "sharelink"));
    if (!ENABLE_FB || !FacebookAgent::getInstance()->canPresentDialogWithParams(params)) {
        menu->cocos2d::Node::removeChild(fbItem);
    }
#endif
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu);
    
    auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(SectionScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(SectionScene::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(SectionScene::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    
    auto listener_keyboard = EventListenerKeyboard::create();
    listener_keyboard->onKeyReleased = [=](EventKeyboard::KeyCode keyCode, Event* event){
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK ||
            keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE)
        {
            _eventDispatcher->pauseEventListenersForTarget(this);
            ModalLayer *ml = ModalLayer::create();
            ml->addQuitConfirm("Buttons/Btn_Cancel.png", "Buttons/Btn_Cancel_Pressed.png", [=](Ref* pSender){
                                    ml->removeFromParentAndCleanup(true);
                                    _eventDispatcher->resumeEventListenersForTarget(this);
                               },
                               "Buttons/Btn_Quit.png", "Buttons/Btn_Quit_Pressed.png", [](Ref* pSender){
                                    Director::getInstance()->end();
                                    exit(0);
                               },
                               "Components/Quit_Alert_BG.png");
            this->addChild(ml,10,10);
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener_keyboard, this);

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    // the admob compeonent is Addapptr sdk, Because the cocos2dx framework support admob sdk, I just replace the admob content to the addapptr content, the name isn't changed. Modifing the name is trouble, the cocos2dx framework access/call the component by the name.
    _admob = dynamic_cast<ProtocolAds*>(PluginManager::getInstance()->loadPlugin("AdsAdmob"));
    
    adInfo["AdmobType"] = "2";
    adInfo["AdmobSizeEnum"] = "6";
#endif
    c_rotateLeft = 0;
    c_rotateRight = 0;
    year = year2 = 1970;
    month = month2 = 1;
    day = day2 = 1;
    rs = kRotateStop;
    modal = NULL;
    isInit = false;
    
    return true;
}

void SectionScene::createCircleTurn()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    v_scene = dynamic_cast<Scene*>(this->getParent());
    
    SpriteLeft = Sprite::create("Components/ZodiacCircleLeft.png");
    bodyLeft = PhysicsBody::createCircle(SpriteLeft->getContentSize().width / 2,
                                         PhysicsMaterial(0.9f, 0.9f, 0.9f));
    SpriteLeft->setPhysicsBody(bodyLeft);
    bodyLeft->setAngularDamping(0.6);
    bodyLeft->setTag(1);
    SpriteLeft->setPosition(Vec2(visibleSize.width / 2.0 - 200, visibleSize.height / 2.0 + 20));
    this->addChild(SpriteLeft);
    
    Node* node = Node::create();
    PhysicsBody* box = PhysicsBody::create(0.8,0.8);
    node->setPhysicsBody(box);
    box->setDynamic(false);
    node->setPosition(Vec2(visibleSize.width / 2.0 - 200, visibleSize.height / 2.0 + 20));
    this->addChild(node);
    
    Vec2 center_p = Vec2(200,200);
    int  radius = 165;
    
    for (int i = 0; i < 12 ; i++){
        
        float sin_v = sin(CC_DEGREES_TO_RADIANS(i*-30));
        
        float cos_v = cos(CC_DEGREES_TO_RADIANS(i*-30));
        
        Vec2 spr_p = Vec2(radius * cos_v + center_p.x, radius * sin_v + center_p.y);
        
        char filename[40];
        sprintf(filename, CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon%d_CN.png":"ZodiacIcons/ZodiacIcon%d.png", i + 1);
        Sprite *spr = Sprite::create(filename);
        spr->setPosition(spr_p);
        SpriteLeft->addChild(spr);
    }
    
    currentSpriteLeft = Sprite::create(CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon1_Big_CN.png":"ZodiacIcons/ZodiacIcon1_Big.png");
    currentSpriteLeft->setPosition(Vec2(visibleSize.width / 2.0 - 200, visibleSize.height / 2.0 + 20));
    this->addChild(currentSpriteLeft);
    
    SpriteRight = Sprite::create("Components/ZodiacCircleRight.png");
    bodyRight = PhysicsBody::createCircle(SpriteRight->getContentSize().width / 2,
                                         PhysicsMaterial(0.9f, 0.9f, 0.9f));
    SpriteRight->setPhysicsBody(bodyRight);
    bodyRight->setAngularDamping(0.6);
    bodyRight->setTag(2);
    SpriteRight->setPosition(Vec2(visibleSize.width / 2.0 + 210, visibleSize.height / 2.0 - 56));
    this->addChild(SpriteRight);
    
    Node* node2 = Node::create();
    PhysicsBody* box2 = PhysicsBody::create(0.8,0.8);
    node2->setPhysicsBody(box2);
    box2->setDynamic(false);
    node2->setPosition(Vec2(visibleSize.width / 2.0 + 210, visibleSize.height / 2.0 - 56));
    this->addChild(node2);
    
    for (int i = 0; i < 12 ; i++){
        
        float sin_v = sin(CC_DEGREES_TO_RADIANS(i*-30));
        
        float cos_v = cos(CC_DEGREES_TO_RADIANS(i*-30));
        
        Vec2 spr_p = Vec2(radius * cos_v + center_p.x, radius * sin_v + center_p.y);
        
        char filename[40];
        sprintf(filename, CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon%d_CN.png":"ZodiacIcons/ZodiacIcon%d.png", i + 1);
        Sprite *spr = Sprite::create(filename);
        spr->setPosition(spr_p);
        SpriteRight->addChild(spr);
    }
    
    currentSpriteRight = Sprite::create(CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon7_Big_CN.png":"ZodiacIcons/ZodiacIcon7_Big.png");
    currentSpriteRight->setPosition(Vec2(visibleSize.width / 2.0 + 210, visibleSize.height / 2.0 - 56));
    this->addChild(currentSpriteRight);
    
    v_scene->getPhysicsWorld()->addJoint(PhysicsJointPin::construct(SpriteLeft->getPhysicsBody(), box, SpriteLeft->getPosition()));
    v_scene->getPhysicsWorld()->addJoint(PhysicsJointPin::construct(SpriteRight->getPhysicsBody(), box2, SpriteRight->getPosition()));
    
    Sprite *comparebox = Sprite::create(CHINA_ZODIAC?"Components/CompareBox_CN.png":"Components/CompareBox.png");
    comparebox->setPosition(Vec2(visibleSize.width/2.0, visibleSize.height / 2.0 - 25));
    this->addChild(comparebox);
    
    isInit = true;
}

void SectionScene::createPicker()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();

    modal = ModalLayer::create();
    this->addChild(modal);
    
    Sprite *bg = Sprite::create("BG/BG_SplashScreen.png");
    bg->setPosition(visibleSize / 2.0);
    modal->addChild(bg);
    
    Sprite *dp_bg = Sprite::create("datepicker/Rq_bg.png");
    dp_bg->setPosition(visibleSize / 2.0);
    modal->addChild(dp_bg);
    
    MenuItemImage *inc_item_1 = MenuItemImage::create("datepicker/rq_3.png", "datepicker/rq_3_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    inc_item_1->setTag(1);
    MenuItemImage *year_1 = MenuItemImage::create("datepicker/rq_6.png", "");
    year_label = Label::createWithBMFont("fonts/OldEnglish.fnt", StringUtils::format("%d", dpLeft?year:year2));
    year_label->setPosition(year_1->getContentSize()/2.0);
    year_1->addChild(year_label);
    MenuItemImage *red_item_1 = MenuItemImage::create("datepicker/rq_9.png", "datepicker/rq_9_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    red_item_1->setTag(2);
    Menu *menu_1 = Menu::create(inc_item_1, year_1, red_item_1, NULL);
    menu_1->alignItemsVerticallyWithPadding(1);
    menu_1->setPosition(Vec2(visibleSize.width / 2.0 + 100, visibleSize.height / 2.0 ));
    modal->addChild(menu_1);
    
    MenuItemImage *inc_item_2 = MenuItemImage::create("datepicker/rq_2.png", "datepicker/rq_2_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    inc_item_2->setTag(3);
    MenuItemImage *month_1 = MenuItemImage::create("datepicker/rq_5.png", "");
    month_label = Label::createWithBMFont("fonts/OldEnglish.fnt", StringUtils::format("%02d", dpLeft?month:month2));
    month_label->setPosition(month_1->getContentSize()/2.0);
    month_1->addChild(month_label);
    MenuItemImage *red_item_2 = MenuItemImage::create("datepicker/rq_8.png", "datepicker/rq_8_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    red_item_2->setTag(4);
    Menu *menu_2 = Menu::create(inc_item_2, month_1, red_item_2, NULL);
    menu_2->alignItemsVerticallyWithPadding(1);
    menu_2->setPosition(Vec2(visibleSize.width / 2.0 , visibleSize.height / 2.0));
    modal->addChild(menu_2);
    
    MenuItemImage *inc_item_3 = MenuItemImage::create("datepicker/rq_1.png", "datepicker/rq_1_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    inc_item_3->setTag(5);
    MenuItemImage *day_1 = MenuItemImage::create("datepicker/rq_4.png", "");
    day_label = Label::createWithBMFont("fonts/OldEnglish.fnt", StringUtils::format("%02d", dpLeft?day:day2));
    day_label->setPosition(day_1->getContentSize()/2.0);
    day_1->addChild(day_label);
    MenuItemImage *red_item_3 = MenuItemImage::create("datepicker/rq_7.png", "datepicker/rq_7_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    red_item_3->setTag(6);
    Menu *menu_3 = Menu::create(inc_item_3, day_1, red_item_3, NULL);
    menu_3->alignItemsVerticallyWithPadding(1);
    menu_3->setPosition(Vec2(visibleSize.width / 2.0 - 100, visibleSize.height / 2.0 ));
    modal->addChild(menu_3);
    
    MenuItemImage *yes_item = MenuItemImage::create("datepicker/Yes.png", "datepicker/Yes_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    yes_item->setTag(7);
    MenuItemImage *no_item = MenuItemImage::create("datepicker/No.png", "datepicker/No_press.png", CC_CALLBACK_1(SectionScene::pickerCallback, this));
    no_item->setTag(8);
    Menu *menu = Menu::create(no_item,yes_item, NULL);
    menu->alignItemsHorizontallyWithPadding(80);
    menu->setPosition(visibleSize.width / 2.0, visibleSize.height * 0.15);
    modal->addChild(menu);
}

void SectionScene::onEnter()
{
    Layer::onEnter();
    if (!isInit){
        this->createCircleTurn();
    }

	this->schedule(CC_SCHEDULE_SELECTOR(SectionScene::scheduleCallback), 0.35);
}

void SectionScene::onExit()
{
    Layer::onExit();
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    _admob->hideAds(adInfo);
    _admob->setAdsListener(NULL);
#endif
    this->unschedule(CC_SCHEDULE_SELECTOR(SectionScene::scheduleCallback));
}

void SectionScene::scheduleCallback(float dt)
{
    int rotateLeft = bodyLeft->getRotation();
	int rotateRight = bodyRight->getRotation();
    rotateLeft = (rotateLeft - 15)%360;
    rotateRight = (rotateRight+165)%360;
    if (rotateLeft < 0) {
        rotateLeft = 360 + rotateLeft;
    }
    if (rotateRight < 0) {
        rotateRight = 360 + rotateRight;
    }
//    log("Rotate %d, %d", rotateLeft, rotateRight);
    if(c_rotateLeft == rotateLeft && c_rotateRight == rotateRight){
        rs = kRotateStop;
    }else if(c_rotateRight != rotateRight){
        int result = rotateRight / 30;
        this->updateCurrentSprite(-1, result);
        this->autoFitCorner(false, 165 - rotateRight);
        rs = kRotateing;
        c_rotateRight = rotateRight;
    }else if(c_rotateLeft != rotateLeft){
        int result = rotateLeft / 30;
        this->updateCurrentSprite(result, -1);
        this->autoFitCorner(true, 345 - rotateLeft);
        rs = kRotateing;
        c_rotateLeft = rotateLeft;
    }
}

void SectionScene::updateCurrentSprite(int left, int right)
{
    if (left != -1) { //无须改变
        int tag = 12 - left;
        char filename[45];
        sprintf(filename, CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon%d_Big_CN.png":"ZodiacIcons/ZodiacIcon%d_Big.png", tag);
        currentSpriteLeft->setTexture(filename);
        if (tag != currentSpriteLeft->getTag()) {
            PlayMusic::PlayShortMusic("sound/tick.wav");
        }
        currentSpriteLeft->setTag(tag);
    }
    if(right != -1){ //无须改变
        int tag = 12 - right;
        char filename[45];
        sprintf(filename, CHINA_ZODIAC?"ZodiacIcons/ZodiacIcon%d_Big_CN.png":"ZodiacIcons/ZodiacIcon%d_Big.png", tag);
        currentSpriteRight->setTexture(filename);
        if (tag != currentSpriteRight->getTag()) {
            PlayMusic::PlayShortMusic("sound/tick.wav");
        }
        currentSpriteRight->setTag(tag);
    }
}

void SectionScene::autoFitCorner(bool isLeft, int corner)
{
    Vector<Node*> childs = isLeft?SpriteLeft->getChildren():SpriteRight->getChildren();
    Vector<Node*>::iterator iter;
    for (iter = childs.begin(); iter!=childs.end(); iter++)
    {
        (*iter)->runAction(RotateTo::create(0.3, corner));
    }
}

bool SectionScene::onTouchBegan(Touch* touch, Event* event)
{
    auto location = touch->getLocation();
    auto arr = v_scene->getPhysicsWorld()->getShapes(location);
    
    PhysicsBody* body = nullptr;
    for (auto& obj : arr)
    {
        if (obj->getBody()->getTag() != 0)
        {
            body = obj->getBody();
            break;
        }
    }
    
    if (body != nullptr)
    {
        Node* mouse = Node::create();
        mouse->setPhysicsBody(PhysicsBody::create(PHYSICS_INFINITY, PHYSICS_INFINITY));
        mouse->getPhysicsBody()->setDynamic(false);
        mouse->setPosition(location);
        this->addChild(mouse);
        PhysicsJointPin* joint = PhysicsJointPin::construct(mouse->getPhysicsBody(), body, location);
        joint->setMaxForce(FORCE_TOUCH * body->getMass());
        v_scene->getPhysicsWorld()->addJoint(joint);
        _mouses.insert(std::make_pair(touch->getID(), mouse));

//        year_label->setString("X");
//        year_label2->setString("X");
//        month_label->setString("X");
//        month_label2->setString("X");
//        day_label->setString("X");
//        day_label2->setString("X");
        return true;
    }
    
    return false;
}

void SectionScene::onTouchMoved(Touch* touch, Event* event)
{
    auto it = _mouses.find(touch->getID());
    
    if (it != _mouses.end())
    {
        it->second->setPosition(touch->getLocation());
    }
}

void SectionScene::onTouchEnded(Touch* touch, Event* event)
{
    auto it = _mouses.find(touch->getID());
    
    if (it != _mouses.end())
    {
        this->removeChild(it->second);
        _mouses.erase(it);
    }
}

void SectionScene::pickerCallback(Ref *pSender)
{
    int tag = ((Node*)pSender)->getTag();
    switch (tag) {
        case 1:
            if (dpLeft) {
                if(year >= 2020) return;
                year++;
                year_label->setString(StringUtils::format("%d", year));
            }else{
                if(year2 >= 2020) return;
                year2++;
                year_label->setString(StringUtils::format("%d", year2));
            }

//            if(CHINA_ZODIAC)checkChinaZodiac();
            break;
        case 2:
            if (dpLeft) {
                if (year <= 1912) return;
                year--;
                year_label->setString(StringUtils::format("%d", year));
            }else{
                if (year2 <= 1912) return;
                year2--;
                year_label->setString(StringUtils::format("%d", year2));
            }

//            if (CHINA_ZODIAC)checkChinaZodiac();
            break;
        case 3:
            if (dpLeft) {
                if (month >= 12 ) return;
                month++;
                month_label->setString(StringUtils::format("%02d", month));
            }else{
                if (month2 >= 12 ) return;
                month2++;
                month_label->setString(StringUtils::format("%02d", month2));
            }
//            if(!CHINA_ZODIAC){
//                checkEuropeZodiac();
//            }
//            else if (month == 1 || month == 2){
//                checkChinaZodiac();
//            }
            break;
        case 4:
            if (dpLeft) {
                if (month <= 1 ) return;
                month--;
                month_label->setString(StringUtils::format("%02d", month));
            }else{
                if (month2 <= 1 ) return;
                month2--;
                month_label->setString(StringUtils::format("%02d", month2));
            }
//            if(!CHINA_ZODIAC){
//                checkEuropeZodiac();
//            }
//            else if (month == 1 || month == 2){
//                checkChinaZodiac();
//            }
            break;
        case 5:{
            if (dpLeft) {
                int days = daysWithYearMonth(year, month);
                if (day >= days) return;
                day++;
                day_label->setString(StringUtils::format("%02d", day));
            }else{
                int days = daysWithYearMonth(year2, month2);
                if (day2 >= days) return;
                day2++;
                day_label->setString(StringUtils::format("%02d", day2));
            }
//            if(!CHINA_ZODIAC){
//                checkEuropeZodiac();
//            }
//            else if (month == 1 || month == 2){
//                checkChinaZodiac();
//            }
        }
            break;
        case 6:
            if (dpLeft) {
                if (day <= 1) return;
                day--;
                day_label->setString(StringUtils::format("%02d", day));
            }else{
                if (day2 <= 1) return;
                day2--;
                day_label->setString(StringUtils::format("%02d", day2));
            }
//            if(!CHINA_ZODIAC){
//                checkEuropeZodiac();
//            }
//            else if (month == 1 || month == 2){
//                checkChinaZodiac();
//            }
            break;
        case 7:
            if(CHINA_ZODIAC){
                checkChinaZodiac();
            }else{
                checkEuropeZodiac();
            }
            modal->removeFromParent();
            modal = NULL;
            break;
        case 8:
            modal->removeFromParent();
            modal = NULL;
            break;
        default:
            break;
    }
}

void SectionScene::checkEuropeZodiac()
{
    //left N * 30 right (N-6) * 30
    if (dpLeft) {
    int zodiac1 = EuropeZodiacFromDate(month, day);
    if (zodiac1 != -1) {
        bodyLeft->setRotation(zodiac1 * -30.0);
    }
    }else{
    int zodiac2 = EuropeZodiacFromDate(month2, day2);
    if (zodiac2 != -1) {
        zodiac2 = zodiac2 - 6;
        if(zodiac2 < 0) zodiac2 = 12 + zodiac2;
        bodyRight->setRotation(zodiac2 * -30.0);
    }
    }
}

void SectionScene::checkChinaZodiac()
{
    //left N * 30 right (N-6) * 30
    if (dpLeft) {
        int zodiac1 = ChinaZodiacFromDate(year, month, day);
        if (zodiac1 != -1) {
            bodyLeft->setRotation(zodiac1 * -30.0);
        }
    }else{
        int zodiac2 = ChinaZodiacFromDate(year2, month2, day2);
        if (zodiac2 != -1) {
            zodiac2 = zodiac2 - 6;
            if(zodiac2 < 0) zodiac2 = 12 + zodiac2;
            bodyRight->setRotation(zodiac2 * -30.0);
        }
    }
}

void SectionScene::menuCallback(Ref* pSender)
{
    MenuItem *item = (MenuItem*)pSender;
    switch (item->getTag()) {
        case 1: //info
            Director::getInstance()->pushScene(InfoScene::createScene());
            break;
        case 2: //gmg
            Director::getInstance()->popScene();
            break;
        case 3: //fb
            {
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
                FacebookAgent::FBInfo params;
                params.insert(std::make_pair("dialog", "sharelink"));
                params.insert(std::make_pair("name", "ZodiacCompare"));
                params.insert(std::make_pair("caption", "ZodiacCompare"));
                params.insert(std::make_pair("description", "ZodiacCompare"));
                params.insert(std::make_pair("link", "http://ZodiacCompare"));
                
                if (FacebookAgent::getInstance()->canPresentDialogWithParams(params))
                {
                    FacebookAgent::getInstance()->dialog(params, [=](int ret ,std::string& msg){
                        CCLOG("%s", msg.c_str());
                    });
                }
#endif
            }
            break;
        case 4: //snd
            stopSound=!stopSound;
            PlayMusic::PlayShortMusic("sound/tick.wav");
            break;
        case 5: //ok
//            if(rs == kRotateStop){
                //12 - （rotateLeft / 30)
                bodyLeft->setResting(true);
                bodyRight->setResting(true);
                SET_LEFT_VALUE(12 - (c_rotateLeft / 30));
                SET_RIGHT_VALUE(12 - (c_rotateRight / 30));
                if (GET_AD_VALUE >= AD_COUNT) {
                    SET_AD_VALUE(0);
                    Scene *scene = BannerScene::createScene();
                    scene->setTag(16);
                    Director::getInstance()->pushScene(scene);
                }else{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
					this->loadingAnimating();
                    _admob->setAdsListener(this);
                    _admob->showAds(adInfo, ProtocolAds::AdsPos::kPosBottom);
#endif
#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
					Director::getInstance()->pushScene(DisplayTScene::createScene());
#endif
                }
                VALUE_FLUSH;
//            }else{
//                bodyLeft->setResting(true);
//                bodyRight->setResting(true);
//                rs = kRotateStop;
//            }
            break;
        case 6://left dp
            dpLeft = true;
            this->createPicker();
            break;
        case 7://right dp
            dpLeft = false;
            this->createPicker();
            break;
        default:
            break;
    }
}

void SectionScene::loadingAnimating()
{
    ModalLayer *ml = ModalLayer::create();
    ml->addLoading("base.png");
    ml->setTag(10);
    this->addChild(ml,10,10);
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
void SectionScene::onAdsResult(AdsResultCode code, const char* msg)
{
    log("OnAdsResult, code : %d, msg : %s", code, msg);
    if (this->getChildByTag(10) != NULL) {
        this->getChildByTag(10)->removeFromParentAndCleanup(true);
    }
    if (code != kAdsReceived) {
        SET_AD_VALUE(GET_AD_VALUE+1);
    }
    Director::getInstance()->pushScene(DisplayTScene::createScene());
}
#endif

int daysWithYearMonth(int v_year, int v_month)
{
    int day = 0;
    
    if (v_month == 1 || v_month == 3 ||
        v_month == 5 || v_month == 7 ||
        v_month == 8 || v_month == 10 || v_month == 12)
    {
        day = 31;
    }
    else if (v_month == 2)
    {
        if ((v_year%4==0 && v_year%100!=0) || v_year%400==0)
        {
            day = 29;
        }
        else
        {
            day = 28;
        }
    }
    else
    {
        day = 30;
    }
    
    return day;
}

static std::string ChinaZodiacDates[] = {"19120218","19130206","19140126","19150214","19160203","19170123","19180211","19190201",
"19200220","19210208","19220128","19230216","19240205","19250125","19260213","19270202","19280123","19290210","19300130",
"19310217","19320206","19330126","19340214","19350204","19360124","19370211","19380131","19390219","19400208","19410127",
"19420215","19430205","19440125","19450213","19460202","19470122","19480210","19490129","19500217","19510206","19520127",
"19530214","19540203","19550124","19560212","19570131","19580218","19590208","19600128","19610215","19620205","19630125",
"19640213","19650202","19660121","19670209","19680130","19690217","19700206","19710127","19720215","19730203","19740123",
"19750211","19760131","19770218","19780207","19790128","19800216","19810205","19820125","19830213","19840202",
"19850220","19860209","19870129","19880217","19890206","19900127","19910215","19920204","19930123","19940210","19950131",
"19960219","19970207","19980128","19990216","20000205","20010124","20020212","20030201","20040122","20050209","20060129",
"20070218","20080207","20090126","20100224","20110203","20120123","20130210","20140131","20150219","20160208","20170128",
"20180216","20190205"};
//return 0-11 ChinaZodiac
int ChinaZodiacFromDate(int year, int month, int day)
{
    std::string date = StringUtils::format("%d%02d%02d", year, month, day);
    int total = 108;//ChinaZodiacDates->size(); array count
    for(int i = 1; i < total ; i ++ ){
        if (date.compare(ChinaZodiacDates[i]) < 0) {
            return ((i-1)%12);
        }
    }
    return -1;// over range
}
//return 0-11 EuropeZodiac
int EuropeZodiacFromDate(int month, int day)
{
    switch(month)
    {
        case 1:
            if(day>=20)
                return 1;
            else
                return 0;
            break;
        case 2:
            if(day>=19)
                return 2;
            else
                return 1;
            break;
        case 3:
            if(day>=21)
                return 3;
            else
                return 2;
            break;
        case 4:
            if(day>=20)
                return 4;
            else
                return 3;
            break;
        case 5:
            if(day>=21)
                return 5;
            else
                return 4;
            break;
        case 6:
            if(day>=22)
                return 6;
            else
                return 5;
            break;
        case 7:
            if(day>=23)
                return 7;
            else
                return 6;
            break;
        case 8:
            if(day>=23)
                return 8;
            else
                return 7;
            break;
        case 9:
            if(day>=23)
                return 9;
            else
                return 8;
            break;
        case 10:
            if(day>=24)
                return 10;
            else
                return 9;
            break;
        case 11:
            if(day>=23)
                return 11;
            else
                return 10;
            break;
        case 12:
            if(day>=20)
                return 0;
            else
                return 11;
            break;
        default:
            
            break;
    }
    return -1; //over range
}
