#include "DisplayTScene.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "FacebookAgent.h"
#include "PluginManager.h"
using namespace cocos2d::plugin;
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
#include "src/Direct3DInterop.h"
using namespace PhoneDirect3DXamlAppComponent;
#endif
#include "Configs.h"
USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
std::wstring CCUtf8ToUnicode(const char * pszUtf8Str, unsigned len/* = -1*/)
{
	std::wstring ret;
	do
	{
		if (!pszUtf8Str) break;
		// get UTF8 string length
		if (-1 == len)
		{
			len = strlen(pszUtf8Str);
		}
		if (len <= 0) break;

		// get UTF16 string length
		int wLen = MultiByteToWideChar(CP_UTF8, 0, pszUtf8Str, len, 0, 0);
		if (0 == wLen || 0xFFFD == wLen) break;

		// convert string  
		wchar_t * pwszStr = new wchar_t[wLen + 1];
		if (!pwszStr) break;
		pwszStr[wLen] = 0;
		MultiByteToWideChar(CP_UTF8, 0, pszUtf8Str, len, pwszStr, wLen + 1);
		ret = pwszStr;
		CC_SAFE_DELETE_ARRAY(pwszStr);
	} while (0);
	return ret;
}
#endif

Scene* DisplayTScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
	auto layer = DisplayTScene::create();
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool DisplayTScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Sprite *bg = Sprite::create("BG/BG_TextScreen.png");
    bg->setPosition(visibleSize/2.0);
    this->addChild(bg);
    
	auto backItem = MenuItemImage::create("Buttons/Btn_Back.png",
                                           "Buttons/Btn_Back_Pressed.png",
                                           CC_CALLBACK_1(DisplayTScene::menuCallback, this));

	backItem->setPosition(Vec2(visibleSize.width * 0.1, visibleSize.height * 0.16));

    
    auto facebookItem = MenuItemImage::create("Buttons/Btn_FB.png",
                                              "Buttons/Btn_FB_Pressed.png",
                                              CC_CALLBACK_1(DisplayTScene::shareToFB, this));
    facebookItem->setPosition(Vec2(visibleSize.width * 0.1, visibleSize.height * 0.32));

    auto menu = Menu::create(backItem, facebookItem, NULL);
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    FacebookAgent::FBInfo params;
    params.insert(std::make_pair("dialog", "sharelink"));
    if (!ENABLE_FB || !FacebookAgent::getInstance()->canPresentDialogWithParams(params)) {
//    if (!ENABLE_FB){
        menu->cocos2d::Node::removeChild(facebookItem);
    }
#endif
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)   
	webView = WebView::create();
	webView->setPosition(Vec2(visibleSize.width / 2.0, visibleSize.height * 0.52 ));
	webView->setContentSize(Size(visibleSize.width * 0.7, visibleSize.height * 0.68));
    webView->setScalesPageToFit(false);
	webView->loadFile(this->getHTMLFileName());
	this->addChild(webView);

    _admob = dynamic_cast<ProtocolAds*>(PluginManager::getInstance()->loadPlugin("AdsAdmob"));
    
    adInfo["AdmobType"] = "1";
    adInfo["AdmobSizeEnum"] = "6";
#endif
   
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	//PhoneDirect3DXamlAppComponent::BroswerEventHelper^ helper = ref new BroswerEventHelper();
	//const std::string str = this->getHTMLFileName();
	//std::wstring ws(CCUtf8ToUnicode(str.c_str(),-1));
	//helper->ShowWebBroswer(false, visibleSize.width * 0.5 - 430, visibleSize.height / 2.0 - 220, 760, 444,ref new Platform::String(ws.data(), ws.length()));
	const char *helpText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, \
		sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.\
	Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut\
	 aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,\
	  vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril \
	  delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod\
	   mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt \
	   lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica\
	   , quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant\
	    sollemnes in futurum.";

	Label *label = Label::createWithTTF(helpText, "MF.ttf", 35);
	label->setMaxLineWidth(visibleSize.width * 0.7);
	label->setTextColor(Color4B::RED);
	label->ignoreAnchorPointForPosition(false);
	label->setAnchorPoint(Vec2(0, 0));
	label->setPosition(Vec2(0, 0));

	Label *labelTitle = Label::createWithTTF("Title", "MF.ttf", 60);
	labelTitle->setTextColor(Color4B::RED);
	labelTitle->setPosition(Vec2(visibleSize.width * 0.35, label->getContentSize().height + 110));

	Label *labelsubTitle = Label::createWithTTF("subTitle", "MF.ttf", 60);
	labelsubTitle->setTextColor(Color4B::RED);
	labelsubTitle->setPosition(Vec2(visibleSize.width * 0.35, label->getContentSize().height + 50));

	Sprite *leftSpr = Sprite::create("htmls/jungfrau.png");
	leftSpr->setPosition(Vec2(visibleSize.width * 0.35 - visibleSize.width * 0.2, label->getContentSize().height + 90));

	Sprite *rightSpr = Sprite::create("htmls/jungfrau.png");
	rightSpr->setPosition(Vec2(visibleSize.width * 0.35 + visibleSize.width * 0.2, label->getContentSize().height + 90));

	auto containerLayer = LayerColor::create(Color4B(255, 255, 255, 0.0), visibleSize.width * 0.7, label->getContentSize().height + 140);
	containerLayer->setPosition(Vec2(0, 0));
	containerLayer->addChild(label);
	containerLayer->addChild(labelTitle);
	containerLayer->addChild(labelsubTitle);
	containerLayer->addChild(leftSpr);
	containerLayer->addChild(rightSpr);

	// NOW create a Scrollview
	auto scrollView = cocos2d::extension::ScrollView::create();
	scrollView->setContentSize(Size(containerLayer->getContentSize().width, containerLayer->getContentSize().height + 140));
	scrollView->ignoreAnchorPointForPosition(false);
	scrollView->setAnchorPoint(Vec2(0.5, 0.5));
	scrollView->setBounceable(false);
	scrollView->setPosition(Point(visibleSize.width * 0.51, visibleSize.height * 0.48));
	scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	scrollView->setViewSize(Size(visibleSize.width * 0.7, visibleSize.height * 0.68));
	scrollView->setContainer(containerLayer);
	this->addChild(scrollView);
	scrollView->setContentOffset(Vec2(0, -containerLayer->getContentSize().height - 140));

#endif

    auto listener = EventListenerKeyboard::create();

    listener->onKeyReleased = [&](EventKeyboard::KeyCode keyCode, Event* event){
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK ||
            keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE) 
        {
            log("back to pop");
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
            _admob->hideAds(adInfo);
#endif
            Director::getInstance()->popScene();
        }
    };

    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void DisplayTScene::onEnter()
{
    Layer::onEnter();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	webView->loadFile(this->getHTMLFileName());
    webView->setScalesPageToFit(false); 
	
	_admob->showAds(adInfo, ProtocolAds::AdsPos::kPosBottom);
#endif
#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
	PhoneDirect3DXamlAppComponent::BroswerEventHelper^ helper = ref new BroswerEventHelper();
	helper->ShowBanner(false, false);
#endif

}

void DisplayTScene::onExit()
{
    Layer::onExit();
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    _admob->hideAds(adInfo);
#endif
#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
	PhoneDirect3DXamlAppComponent::BroswerEventHelper^ helper = ref new BroswerEventHelper();
	helper->ShowBanner(true, false);
	helper->ShowWebBroswer(true, 0, 0, 0, 0, "");
#endif
}

std::string DisplayTScene::getHTMLFileName()
{
    char filename[45];
    std::string language_part;
    std::string zodiac_part_1,zodiac_part_2;
    
    int leftValue = GET_LEFT_VALUE;
    int rightValue = GET_RIGHT_VALUE;
    if(IS_ENGLISH){ // english
        language_part = "en";
    }
    else{ // german
        language_part = "de";
    }
    
    switch (leftValue) {
        case 1:
            zodiac_part_1 = "widder";
            break;
        case 2:
            zodiac_part_1 = "stier";
            break;
        case 3:
            zodiac_part_1 = "zwilling";
            break;
        case 4:
            zodiac_part_1 = "krebs";
            break;
        case 5:
            zodiac_part_1 = "loewe";
            break;
        case 6:
            zodiac_part_1 = "jungfrau";
            break;
        case 7:
            zodiac_part_1 = "waage";
            break;
        case 8:
            zodiac_part_1 = "skorpion";
            break;
        case 9:
            zodiac_part_1 = "schuetze";
            break;
        case 10:
            zodiac_part_1 = "steinbock";
            break;
        case 11:
            zodiac_part_1 = "wassermann";
            break;
        case 12:
            zodiac_part_1 = "fische";
            break;
        default:
            break;
    }
    
    switch (rightValue) {
        case 1:
            zodiac_part_2 = "widder";
            break;
        case 2:
            zodiac_part_2 = "stier";
            break;
        case 3:
            zodiac_part_2 = "zwilling";
            break;
        case 4:
            zodiac_part_2 = "krebs";
            break;
        case 5:
            zodiac_part_2 = "loewe";
            break;
        case 6:
            zodiac_part_2 = "jungfrau";
            break;
        case 7:
            zodiac_part_2 = "waage";
            break;
        case 8:
            zodiac_part_2 = "skorpion";
            break;
        case 9:
            zodiac_part_2 = "schuetze";
            break;
        case 10:
            zodiac_part_2 = "steinbock";
            break;
        case 11:
            zodiac_part_2 = "wassermann";
            break;
        case 12:
            zodiac_part_2 = "fische";
            break;
        default:
            break;
    }
    
    sprintf(filename, (CHINA_ZODIAC?"htmls/%s_%s_%s_CN.html":"htmls/%s_%s_%s.html"),
            zodiac_part_1.c_str(),
            zodiac_part_2.c_str(),
            language_part.c_str());
    if(!FileUtils::getInstance()->isFileExist(filename)){
        sprintf(filename, (CHINA_ZODIAC?"htmls/%s_%s_%s_CN.html":"htmls/%s_%s_%s.html"),
                zodiac_part_2.c_str(),
                zodiac_part_1.c_str(),
                language_part.c_str());
    }
    log("load html: %s",filename);
    return std::string(filename);
}

void DisplayTScene::menuCallback(Ref* pSender)
{
	Director::getInstance()->popScene();
}

void DisplayTScene::shareToFB(cocos2d::Ref* pSender)
{
    log("shareToFB");
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    FacebookAgent::FBInfo params;
    params.insert(std::make_pair("name", "ZodiacCompare"));
    params.insert(std::make_pair("description", "ZodiacCompare"));
    
    FacebookAgent::getInstance()->dialog(params, [=](int ret ,std::string& msg){
        log("%s", msg.c_str());
    });
#endif
}

