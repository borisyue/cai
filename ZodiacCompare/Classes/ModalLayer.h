#ifndef __MODALLAYER_H__
#define __MODALLAYER_H__

#include "cocos2d.h"
USING_NS_CC;

class ModalLayer : public cocos2d::LayerColor
{
public:

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    void addQuitConfirm(const std::string& img1, const std::string& selectedimg1, const ccMenuCallback& callback1,
                                    const std::string& img2, const std::string& selectedimg2, const ccMenuCallback& callback2,
                                    const std::string& bgimg);
    void addLoading(const std::string& loadingimg);
    // implement the "static create()" method manually
    CREATE_FUNC(ModalLayer);
};

#endif // __HELLOWORLD_SCENE_H__

//=========================usage====================
//ModalLayer *ml = ModalLayer::create();
//ml->addLoading("back.png");
//ml->addQuitConfirm("CloseNormal.png", "CloseSelected.png", [=](Ref* pSender){
//    ml->removeFromParentAndCleanup(true);
//},
//                   "CloseNormal.png", "CloseSelected.png", [](Ref* pSender){
//                       Director::getInstance()->end();
//                       exit(0);
//                   },
//                   "HelloWorld.png");
//this->addChild(ml,10,10);
