//
//  LiquidMVideoViewController.h
//  LiquidM-iOS-SDK
//
//  Created by Khaterine Castellano on 12/9/13.
//  Copyright (c) 2013 LiquidM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "LiquidMAbstractAdViewController.h"

/*!
 This is the protocol that has to be implemented to set the
 [LiquidMAbstractVideoViewController delegate] property.
 */
@protocol LiquidMAbstractVideoViewControllerDelegate <LiquidMAbstractAdViewControllerDelegate>

@end

@interface LiquidMAbstractVideoViewController : LiquidMAbstractAdViewController

/*!
 @abstract The delegate that gets notified about loading and presentation
 events.
 
 @discussion It is adviced to use the delegate to be at least informed about the
 basic loading events in order to do proper presentation.
 
 @see LiquidMAbstractVideoViewControllerDelegate
 */
@property (nonatomic, weak) id<LiquidMAbstractVideoViewControllerDelegate> delegate;

/*!
 @name Video Playing
 */

/*!
 @abstract Starts the playback of the video possibly with an ad beforehand.
 */
- (void)play;

/*!
 @abstract Pauses the current playing video.
 */
- (void)pause;

/*!
 @abstract Stops the current video playing.
 */
- (void)stop;

@end
