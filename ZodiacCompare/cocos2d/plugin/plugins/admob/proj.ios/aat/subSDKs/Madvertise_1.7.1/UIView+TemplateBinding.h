//
//  UIView+TemplateBinding.h
//  TemplatePrototype
//
//  Created by Khaterine Castellano on 04/06/14.
//
//

#import <UIKit/UIKit.h>
#import "LiquidMNativeAd.h"

@interface UIView (TemplateBinding)

- (void)bindWithObject:(LiquidMNativeAd *)obj;

@end
