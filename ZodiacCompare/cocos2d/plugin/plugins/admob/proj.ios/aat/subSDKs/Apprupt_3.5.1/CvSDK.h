//
//  CvSDK2.h
//  CvSDK2
//
//  Created by Michał Kluszewski on 11.05.2012.
//  Copyright (c) 2012 apprupt GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


#define kCvI18N_Call @"Anrufen"
#define kCvI18N_Cancel @"Abbrechen"
#define kCvI18N_OK @"OK"
#define kCvI18N_TakePhoto @"Foto aufnehmen"
#define kCvI18N_RecordVideo @"Video aufnehmen"
#define kCvI18N_ChooseFromLibrary @"Vorhandenes auswählen"
#define kCvI18N_UseFrontCamera @"Frontkamera"
#define kCvI18N_UseRearCamera @"Rückseitekamera"
#define kCvI18N_EventAddedTitle @"Ereignis hinzugefügt"
#define kCvI18N_EventAdded @"Die \"%@\" Erinnerung wurde erfolgreich zu Ihrem Kalender hinzugefügt."
#define kCvI18N_EventsAdded @"%d Ereignis(se) zum Kalender hinzugefügt."
#define kCvI18N_ReminderAddedTitle @"Erinnerung hinzugefügt"
#define kCvI18N_ReminderAdded @"Die \"%@\" Erinnerung wurde erfolgreich erstellt."
#define kCvI18N_RemindersAdded @"%d Erinnerung(en) hinzugefügt."
#define kCvI18N_ClipkitPlayerMsg @"Noch {COUNTDOWN} Sekunden."


typedef NS_OPTIONS(NSUInteger, CvSDKDebugLevel) {
    CvSDKDebugLevelQuiet    = 0,
    CvSDKDebugLevelErrors   = 1,
    CvSDKDebugLevelWarnings = 3,
    CvSDKDebugLevelInfo     = 7,
    CvSDKDebugLevelVerbose  = 15,
    
    CvSDKDebugLevelAll      = 15,
};


typedef NS_ENUM(NSUInteger, CvAdSpaceAnimationType) {
    CvAdSpaceAnimationTypeDefault,
    CvAdSpaceAnimationTypeNone,
    CvAdSpaceAnimationTypeLeftRight,
    CvAdSpaceAnimationTypeRightLeft,
    CvAdSpaceAnimationTypeTopBottom,
    CvAdSpaceAnimationTypeBottomTop,
    CvAdSpaceAnimationTypeFade
};


typedef NS_ENUM(NSUInteger, CvGender) {
    CvGenderUnknown,
    CvGenderMale,
    CvGenderFemale
};

typedef NS_ENUM(NSUInteger, CvRelationshipStatus) {
    CvRelationshipStatusUnknown,
    CvRelationshipStatusSingle,
    CvRelationshipStatusInRelationship,
    CvRelationshipStatusEngaged,
    CvRelationshipStatusMarried,
    CvRelationshipStatusSeparated,
    CvRelationshipStatusDivorced,
    CvRelationshipStatusWidowed
};

@class CvAdSpace;


@protocol CvInterstitialDelegate <NSObject>

@optional
- (void) cvInterstitialWillAppear:(NSString *)adSpaceId;
- (void) cvInterstitialDidAppear:(NSString *)adSpaceId;
- (void) cvInterstitialWillDisappear:(NSString *)adSpaceId;
- (void) cvInterstitialDidDisappear:(NSString *)adSpaceId;
- (void) cvInterstitialDidFailWithError:(NSError *)error;
- (void) cvInterstitialDidReceiveFirstTap:(NSString *)adSpaceId;

@end

@protocol CvInterstitialPresenter <NSObject>
@optional
- (void) presentCvInterstitialModally:(UIViewController *)cvInterstitial;
- (void) dismissCvInterstitial:(UIViewController *)cvInterstitial;
@end

@protocol CvAdSpaceDelegate <NSObject>
@required
- (CGRect) cvAdSpace:(CvAdSpace *)adSpace willChangeViewSize:(CGSize)newSize;
@optional
- (void) cvAdSpaceWillShowOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceWillHideOverlay:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedFirstAd:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedFirstAd:(CvAdSpace *)adSpace withSize:(CGSize)size;
- (void) cvAdSpaceLoadedAd:(CvAdSpace *)adSpace;
- (void) cvAdSpaceLoadedAd:(CvAdSpace *)adSpace withSize:(CGSize)size;
- (void) cvAdSpaceAdDidAppear:(CvAdSpace *)adSpace first:(BOOL)first;
- (void) cvAdSpace:(CvAdSpace *)adSpace failedWithError:(NSError *)error;
- (void) presentCvLandingPageControllerModally:(UIViewController *)controller;
- (void) dismissCvLandingPageController:(UIViewController *)controller;
- (void) cvAdSpaceDidReceiveFirstTap:(CvAdSpace *)adSpace;
@end



@interface CvAdSpaceOptions : NSObject 

@property (nonatomic,strong) NSString * categories;
@property (nonatomic,strong) NSString * keywords;
@property (nonatomic,assign) CvAdSpaceAnimationType animationType;
@property (nonatomic,assign) NSTimeInterval minRequestsInterval;
@property (nonatomic,assign) BOOL clearSpaceBeforeLoad;

@end


typedef void (^CvAdsAvailableCallback)(BOOL available);
typedef void (^CvAdPrefetchCallback)(BOOL success, NSError *error);

@interface CvAdSpace : NSObject 

@property (nonatomic,readonly,strong) NSString * adSpaceId;
@property (nonatomic,readonly,strong) CvAdSpaceOptions * options;
@property (nonatomic,readonly) UIView * view;
@property (nonatomic,strong) id<CvAdSpaceDelegate> delegate;
@property (nonatomic,readonly) CGSize size;
@property (nonatomic,assign) NSUInteger cacheMaxSize;
@property (nonatomic,assign) BOOL keepCacheFull;

- (BOOL) load;
- (void) checkAdsAvailable:(CvAdsAvailableCallback)completion;
- (void) prefetchAd:(CvAdPrefetchCallback)completion;
- (void) resume;
- (void) pause;
- (void) clear;

@end


@interface CvAudience : NSObject

@property (nonatomic,assign) CvGender gender;
@property (nonatomic,strong) NSDate *birthday;
@property (nonatomic,assign) NSUInteger age;
@property (nonatomic,assign) CvRelationshipStatus relationshipStatus;
@property (nonatomic,assign) NSInteger numberOfChildren;

@end


@interface CvSDK : NSObject

@property (nonatomic,readonly,strong) NSString * piKey;
@property (nonatomic,readonly,strong) CvAdSpaceOptions * defaultOptions;
@property (nonatomic,readonly,strong) CvAudience *audience;
@property (nonatomic,assign) BOOL useSecureConnection;


+ (CvSDK *) sharedInstance;
+ (NSString *) version;
+ (void) setDebugLevel:(CvSDKDebugLevel)level;
- (void) regeneratePiKey;
- (void) enableGeoLocation;
- (void) disableGeoLocation;
- (void) enableHeadingMonitor;
- (void) disableHeadingMonitor;
- (void) updateGeoLocation:(CLLocation *)location;
- (void) updateHeading:(CLHeading *)heading;
- (CvAdSpace *) createAdSpace:(NSString *)adSpaceId withDelegate:(id<CvAdSpaceDelegate>)delegate;
- (CvAdSpace *) createAdSpace:(NSString *)adSpaceId withDelegate:(id<CvAdSpaceDelegate>)delegate andViewFrame:(CGRect)frame;
- (CvAdSpace *) retrieveAdSpace:(NSString *)adSpaceId;
- (void) startInterstitial:(NSString *)adSpaceId withHostController:(UIViewController<CvInterstitialPresenter> *)controller;
- (void) startInterstitial:(NSString *)adSpaceId withHostController:(UIViewController<CvInterstitialPresenter> *)controller andDelegate:(id<CvInterstitialDelegate>)delegate;
- (void) prefetchInterstitial:(NSString *)adSpaceId completion:(CvAdPrefetchCallback)completion;
- (void) checkInterstitialAvailable:(NSString *)adSpaceId completion:(CvAdsAvailableCallback)completion;
- (void) pauseAdSpaces:(NSArray *)spaces;
- (void) resumeAdSpaces:(NSArray *)spaces;
- (void) destroyAdSpace:(NSString *)adSpaceId;
- (void) destroyAdSpaces:(NSArray *)spaces;
- (void) forI18NString:(NSString *)i18nString setTranslation:(NSString *)translation;
+ (NSString *) i18n:(NSString *)i18nString;

@end


#if !__has_feature(objc_arc) && __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_5_0
#error "ARC is disabled. If you are sure that ARC symbols are included in this project (ARCLite library) you can remove that line."
// Probably you should use CvSDK library version for non-ARC projects.
// To add arc symbols into project add -fobjc-arc flag into 'Other Linker Flags' under 'Build Settings'.
#endif

