//
//  LiquidMNativeViewGenerator.h
//  LiquidM-iOS-SDK
//
//  Created by Khaterine Castellano on 06/06/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiquidMNativeAd.h"
#import "UIView+TemplateBinding.h"

/*!
 This controller fills the storyboard assets with the information obtained from the native ad parameter.
 */

@interface LiquidMNativeViewGenerator : UIView

/*!
 @abstract Creates a LiquidMNativeViewGenerator with all necessary information.
 
 @param storyboardName The storyboard that contains the UI with the assets that will be filled with information
 
 @param nativeAd The loaded native ad that contains all the information.
 
 @return Fully initialized instance of a LiquidMNativeViewGenerator
 */
- (id)initWithStoryboardName:(NSString *)storyboardName
                 andNativeAd:(LiquidMNativeAd *)nativeAd;

/*!
 @abstract This method returns the rendered UIView with all the assets filled with the information from the native ad.
 */
- (UIView *)renderedAd;

@end
