//
//  LiquidMAbstractPresentableAdViewController.h
//  LiquidM-iOS-SDK
//
//  Created by Khaterine Castellano on 30/05/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import "LiquidMAbstractAdViewController.h"

/*!
 This is the protocol that has to be implemented to set the
 [LiquidMAdViewController delegate] property.
 */

@protocol LiquidMAbstractPresentableAdViewControllerDelegate <LiquidMAbstractAdViewControllerDelegate>

@end


@interface LiquidMAbstractPresentableAdViewController : LiquidMAbstractAdViewController <LiquidMAbstractAdControllerDelegate>

/*!
 @abstract The delegate that gets notified about loading and presentation
 events.
 
 @discussion It is adviced to use the delegate to be at least informed about the
 basic loading events in order to do proper presentation.
 
 @see LiquidMAdViewControllerDelegate
 */
@property (nonatomic, weak) id<LiquidMAbstractPresentableAdViewControllerDelegate> delegate;

/*!
 @abstract Displays the video interstitial view on the screen.
 
 @discussion This method should always be called after receiving the ad from the
 server. For video interstitials this will present the ad on screen
 as well.
 
 @return The returned boolean value confirms if the action can be done or not.
 
 @see [LiquidMAbstractControllerDelegate controllerDidReceiveAd:]
 */
- (BOOL)presentAd;

/*!
 @abstract Displays the ad view on the screen using the specified
 UIViewController.
 
 @discussion This alters the `rootViewController` and then will call presentAd.
 
 @see presentAd
 */
- (BOOL)presentAdOn:(UIViewController *)vc;

/*!
 @abstract Method used to dismiss the fullscreen ads.
 
 @discussion Besides dismissing the view containing the ad, this method calls the delegates methods that register the willDismiss and didDismiss.
 */
- (void)dismiss;


@end
