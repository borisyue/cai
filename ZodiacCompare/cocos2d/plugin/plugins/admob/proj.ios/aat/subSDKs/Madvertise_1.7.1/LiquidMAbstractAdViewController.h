//
//  LiquidMAbstractAdViewController.h
//  LiquidM-iOS-SDK
//
//  Created by Conrad Calmez on 19/03/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LiquidMAbstractAdController.h"

@class LiquidMAbstractAdViewController;

/*!
 This protocol defines the interface for LiquidM ad view controller delegates.
 */
@protocol LiquidMAbstractAdViewControllerDelegate <LiquidMAbstractAdControllerDelegate>

/*!
 @name Loading
 */

@required

/*!
 @abstract Method called when the loading of the ad is finished.
 
 @param controller The LiquidMAbstractController that received the ad
 */
- (void)controllerDidReceiveAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called when the loading of the ad failed.
 
 @param controller The LiquidMAbstractController that failed to receive the ad
 
 @param error The error that occured when loading an ad
 */
- (void)controller:(LiquidMAbstractAdViewController *)controller
        didFailToReceiveAdWithError:(NSError*)error;

/*!
 @name Presentation
 */

@optional

/*!
 @abstract Method called just before the ad's view will appear on screen.
 
 @param controller The LiquidMAdViewController that will present the ad
 */
- (void)controllerWillPresentAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called after the ad's view appeared on screen.
 
 @param controller The LiquidMAdViewController that did present the ad
 */
- (void)controllerDidPresentAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called just before the ad's view will disappear from screen.
 
 @param controller The LiquidMAdViewController that will dismiss the ad
 */
- (void)controllerWillDismissAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called after the ad's view disappeared from screen.
 
 @param controller The LiquidMAdViewController that did dismiss the ad
 */
- (void)controllerDidDismissAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called after the user clicks on the ad.
 
 @param controller The LiquidMAdViewController that did register the click
 */
- (void)controllerDidClickAd:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called just before the browser modal view will appear on screen.
 
 @param controller The LiquidMAdViewController that will present the modal view
 */
- (void)controllerWillPresentModalView:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called just before the modal view will disappear from screen.
 
 @param controller The LiquidMAdViewController that will dismiss the modal view
 */
- (void)controllerWillDismissModalView:(LiquidMAbstractAdViewController *)controller;

/*!
 @abstract Method called after the modal view disappeared from screen.
 
 @param controller The LiquidMAdViewController that did dismiss the modal view
 */
- (void)controllerDidDismissModalView:(LiquidMAbstractAdViewController *)controller;

@end

/*!
 This controller gathers general behavior of LiquidM ad view controllers that
 also take care of rendering the loaded ads and have therefore UIViews as well
 to display the ready made ads.
 
 The API corresponds to the LiquidMAbstractAdController's API.
 */
@interface LiquidMAbstractAdViewController : UIViewController

/*!
 @abstract The delegate that gets notified about loading events.
 
 @discussion It is adviced to use the delegate to be at least informed about the
 basic loading events in order to do proper presentation.
 
 @see LiquidMAbstractAdViewControllerDelegate
 */
@property (nonatomic, weak) id<LiquidMAbstractAdViewControllerDelegate> delegate;

/*!
 @abstract Ad class used for the ads to load and display an ad.
 
 @discussion It can have one of the values listed in the ad class table in the
 overview section. Setting this property implies the related ad size.
 
 @warning For banner ads this property is discouraged in favor of
 [LiquidMAdViewController adSize].
 
 @see [LiquidMAdViewController adSize]
 */
@property (nonatomic) LiquidMAdClass currentAdClass;

/*!
 @abstract The actual token that is used to load an ad.
 
 @discussion If you leave this property untouched the SDK will try to load a
 token dictionary from the app's plist file. On the other hand if you set it the
 token you provided will be used directly for ad loading.
 You should choose to either use tokenTag or token for choosing a token for ad
 loading.
 
 @see tokenTag
 */
@property (nonatomic, copy) NSString *token;

/*!
 @abstract The tag that is being used to search for an app token in the app's
 plist file.
 
 @discussion App tokens should be added to the plist file in a dictionary called
 `LiquidMAppIDs` with its tag as a key and the token itself as a value. For
 testing purposes you should at least add the "default" tag's token to properly
 load an ad.
 */
@property (nonatomic, copy) NSString *tokenTag;

/*!
 @abstract The location that is used for location based ads.
 
 @discussion If the location gets not set it will not be used for the ad
 request. It can be set on creation of a banner controller via the
 `LiquidMControllerOptionLocation` option.
 
 @see [LiquidMAdViewController controllerWithRootViewController:adSize:options:]
 */
@property (nonatomic, copy) CLLocation *location;

/*!
 @name Keyword Management
 */

/*!
 @abstract Returns all currently set keywords used for ad loading.
 */
@property (nonatomic, readonly) NSArray *keywords;

/*!
 @abstract Adds a keyword to the list of keywords.
 
 @param keyword The keyword to add
 */
- (void)addKeyword:(NSString *)keyword;

/*!
 @abstract Adds all keywords of an array to the list of keywords.
 
 @param keywords An array of keywords to add
 */
- (void)addKeywords:(NSArray *)keywords;

/*!
 @abstract Removes a keyword from the list of keywords.
 
 @param keyword The keyword to remove
 */
- (void)removeKeyword:(NSString *)keyword;

/*!
 @abstract Removes all keywords of an array from the list of keywords.
 
 @param keywords An array of keywords to remove
 */
- (void)removeKeywords:(NSArray *)keywords;

/*!
 @abstract Empties the list of keywords.
 */
- (void)removeAllKeywords;

@end
