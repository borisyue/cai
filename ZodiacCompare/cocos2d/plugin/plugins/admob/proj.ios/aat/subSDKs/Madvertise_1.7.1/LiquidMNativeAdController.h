//
//  LiquidMNativeAdController.h
//  LiquidM-iOS-SDK
//
//  Created by Conrad Calmez on 20/03/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import "LiquidMAbstractAdViewController.h"
#import "LiquidMNativeAd.h"

@class LiquidMNativeAdController;

/*!
 This protocol defines the interface for LiquidM native ad controller delegates.
 */
@protocol LiquidMNativeAdControllerDelegate <LiquidMAbstractAdControllerDelegate>

/*!
 @abstract Method called when the loading of the ad is finished.
 
 @param controller The LiquidMNativeController that received the ad
 @param ad The LiquidMNativeAd that has been loaded
 */
- (void)controller:(LiquidMNativeAdController *)controller
      didReceiveAd:(LiquidMNativeAd *)ad;

@end

/*!
 This controller loads native ads and hands them out when loaded.
 */
@interface LiquidMNativeAdController : LiquidMAbstractAdController

/*!
 @abstract The delegate that gets notified about loading events.
 
 @discussion It is adviced to use the delegate to be at least informed about the
 basic loading events in order to do proper presentation.
 
 @see LiquidMNativeAdControllerDelegate
 */
@property (nonatomic, weak) id<LiquidMNativeAdControllerDelegate> delegate;

/*!
 @abstract The current ad that was loaded.
 
 @discussion This property changes when an ad is loaded. Initially when no ad
 was loaded its value will be `nil`.
 */
@property (nonatomic, readonly) LiquidMNativeAd *currentAd;

/*!
 @abstract Creates a LiquidMNativeAdController with all necessary information.
 
 @param schemaName The schema that the requested ad should belong to
 
 @return Fully initialized instance of a LiquidMNativeAdController
 
 @see controllerWithOptions:
 */
+ (LiquidMNativeAdController *)controllerWithSchemaName:(NSString *)schemaName;

/*!
 @abstract Creates a LiquidMNativeAdController with all necessary information.
 
 @param schemaName The schema that the requested ad should belong to
 
 @param options A dictionary that contains optional parameters.  It can
 optionally contain the one or more of the key-value pairs listed in the
 overview section.
 
 @return Fully initialized instance of a LiquidMNativeAdController
 */
+ (LiquidMNativeAdController *)controllerWithSchemaName:(NSString *)schemaName
                                                options:(NSDictionary *)options;

/*!
 @abstract Manually reloads the ads.
 
 @discussion This method will do nothing if autoreload is turned on.
 */
- (void)reloadAd;

@end
