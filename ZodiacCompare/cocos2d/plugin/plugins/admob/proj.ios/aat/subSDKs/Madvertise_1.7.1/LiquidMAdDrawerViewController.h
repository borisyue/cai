//
//  LiquidMAdTrayViewController.h
//  LiquidM-iOS-SDK
//
//  Created by Conrad Calmez on 23/05/14.
//  Copyright (c) 2014 LiquidM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiquidMAbstractAdViewController.h"

@interface LiquidMAdDrawerViewController : UIViewController

/*!
 @abstract Creates a LiquidMAdDrawerViewController with a MMA sized ad attached
 to it.
 
 @discussion If you need to parametrize ad loading (such as setting the token,
 because per default this is loading on TestTokn) use
 drawerWithAdViewController:.
 
 @see drawerWithAdViewController:
 */
+ (LiquidMAdDrawerViewController *)drawer;

/*!
 @abstract Creates a LiquidMAdDrawerViewController with an ad loaded by the
 controller passed to it.
 
 @param advc The LiquidMAbstractAdViewController used for loading of the ad 
 attached to the LiquidMAdDrawerViewController.
 
 @see drawer
 */
+ (LiquidMAdDrawerViewController *)drawerWithAdViewController:(LiquidMAbstractAdViewController *)advc;

/*!
 @abstract Determines if the ad in the ad tray is visible.
 */
@property (nonatomic, readonly, getter = isOpen) BOOL open;

/*!
 @abstract Shows the ad in the tray if not yet visible.
 */
- (void)open;

/*!
 @abstract Hides the ad in the tray if not yet hidden.
 */
- (void)close;

/*!
 @abstract Toggles the visibility of the ad in the tray.
 */
- (void)toggleVisibility;

/*!
 @abstract Has to be called in order to inform the drawer that the ad controller
 successfully received an ad.
 
 @param advc The same LiquidMAbstractAdViewController that was passed during
 initialization.
 
 @discussion You will have to call this method when you don't set the drawer as
 the ad controller's delegate. You should call this method right after having
 called presentAd on you ad controller. Calling this method will make it
 possible to open the drawer since ad content is available to display.
 You only need to call this method once since it is assume that afterwards there
 is always ad content available to display.
 */
- (void)controllerHasContentReady:(LiquidMAbstractAdViewController *)advc;

@end
