//
//  AATConfig.m
//  IntentKit
//
//  Created by Daniel Brockhaus on 30.01.13.
//  Copyright (c) 2013 Daniel Brockhaus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

bool aat_use_Adfonic=YES;
bool aat_use_Admob=YES;
bool aat_use_Addapptr=YES;
bool aat_use_AdMarvel=YES;
bool aat_use_AppLift=YES;
bool aat_use_Applovin=YES;
bool aat_use_Apprupt=YES;
bool aat_use_DFP=YES;
bool aat_use_EMS=YES;
bool aat_use_Facebook=YES;
bool aat_use_GroupM=YES;
bool aat_use_iAd=YES;
bool aat_use_Inmobi=YES;
bool aat_use_LoopMe=YES;
bool aat_use_Madvertise=YES;
bool aat_use_MdotM=YES;
bool aat_use_Millennial=YES;
bool aat_use_MobFox=YES;
bool aat_use_mOcean=YES;
bool aat_use_MPerf=YES;
bool aat_use_Playhaven=YES;
bool aat_use_RTB1=YES;
bool aat_use_RTB2=YES;
bool aat_use_Smaato=YES;
bool aat_use_SmartAd=YES;
bool aat_use_Smartclip=YES;
bool aat_use_Vungle=NO;
bool aat_use_Yume=YES;


#if 0   // please enable this when NOT using Millennial
NSString *MillennialMediaAdWasTapped;
NSString *MillennialMediaAdObjectKey;
NSString *MillennialMediaAdModalWillAppear;
NSString *MillennialMediaAdModalDidDismiss;
#endif
