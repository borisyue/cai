//
//  AATKit.h
//
//  Created by Daniel Brockhaus on 04.04.12.
//  Copyright (c) 2012-2013 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef AATKITDELEGATE
#define AATKITDELEGATE
@protocol AATKitDelegate<NSObject>

@optional
- (void) AATKitHaveAd:(id) placement;
- (void) AATKitNoAds:(id) placement;	
- (void) AATKitShowingEmpty:(id) placement; // ONLY works for banner placements, NOT for fullscreen
- (void) AATKitPauseForAd;
- (void) AATKitResumeAfterAd;
- (bool) AATKitAppWillHandleURL:(NSURLRequest*) url;	// ignore unless told otherwise by AddApptr support
- (void) AATKitUserEarnedIncentive;						// ignore unless told otherwise by AddApptr support
@end
#endif

#ifndef AATKITBANNERALIGN
#define AATKITBANNERALIGN
typedef enum AATKitBannerAlign {
    AATKitBannerTop,
    AATKitBannerBottom,
    AATKitBannerCenter
} AATKitBannerAlign;
#endif

#ifndef AATKITADTYPE
#define AATKITADTYPE
typedef enum AATKitAdType {
    // Generic
    AATKitBanner320x53,
    AATKitBanner768x90,
    AATKitBanner300x250,
    AATKitBanner468x60,
    AATKitFullscreen,
    // iAd only
    AATKitBanner480x32,
    AATKitBanner1024x66,
    // House Ad only
    AATKitBanner300x200,
    AATKitBanner90x80,
    AATKitBanner200x200
} AATKitAdType;
#endif

#ifndef AATKITCLASS
#define AATKITCLASS
@interface AATKit : NSObject

// init
+ (void) initWithViewController:(UIViewController*)viewcon andDelegate:(id<AATKitDelegate>)delegate;
+ (void) setViewController:(UIViewController*)con;
+ (void) setDelegate:(id<AATKitDelegate>)delegate;
+ (NSString *) getVersion; // AATKit Version Number, eg. 0104 equals version 1.04
+ (void) debug:(bool) flag;
+ (void) extendedDebug: (BOOL) yesOrNo;
+ (void) debugShake:(bool) flag;
+ (void) enableTestModeWithID:(int) myID;
+ (void) setInitialRules: (NSString *) rules;
+ (void) enableRulesCaching;

// placements
+ (id) createPlacementWithName:(NSString *) placementName andType:(AATKitAdType) type;
+ (id) getPlacementWithName:(NSString *) placementName;
+ (void) startPlacementAutoReload:(id) placement;
+ (void) stopPlacementAutoReload:(id) placement;
+ (void) setPlacementViewController:(UIViewController*)con forPlacement:(id) placement;
+ (void) reloadPlacement:(id) placement; // must not call this if autoreload is enabled for this placement
+ (void) setPlacementSubID:(int) subID forPlacement:(id) placement;

// banner only
+ (void) startPlacementAutoReloadWithSeconds:(int) seconds forPlacement:(id) placement;
+ (UIView*) getPlacementView:(id) placement;
+ (CGSize) getPlacementContentSize:(id) placement;
+ (void) setPlacementAlign:(AATKitBannerAlign) align forPlacement:(id) placement;
+ (void) setPlacementPos:(CGPoint) pos forPlacement:(id) placement;
+ (void) setPlacementDefaultImage:(UIImage *) image forPlacement:(id) placement;

// fullscreen only
+ (bool) showPlacement:(id) placement;

// promo specials
+ (void) enablePromo;       // enables display of promotional fullscreen ads. THIS IS THE ONLY CALL NEEDED
+ (void) disablePromo;      // disables promotional full screen ads. use this to avoid displaying them when inappropriate
+ (void) preparePromo;      // prepares to display promotional fullscreen ads (ie. load and cache) but do not display yet
                            // enablePromo does this automatically
+ (void) showPromo;         // will show a promo if one is available. you need to call preparePromo first.
+ (void) setPromoViewController:(UIViewController*)viewcon; // use this only if you are using per-placement view controllers

@end
#endif
