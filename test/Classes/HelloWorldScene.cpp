#include "HelloWorldScene.h"
#include "CircleTurn.h"
#include "ui/CocosGUI.h"
#include "ModalLayer.h"

USING_NS_CC;
using namespace cocos2d::plugin;

using namespace cocos2d::experimental::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
//    scene->getPhysicsWorld()->setGravity(Vec2(0.5,0.2));
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    
    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    

    //    CircleTurn *circle = CircleTurn::create("back.png");
    //    circle->setPosition(visibleSize/2.0);
    //    this->addChild(circle);
    
    //    WebView *webView = WebView::create();
    //    webView->setPosition(visibleSize/2);
    //    webView->setContentSize(Size(visibleSize.width , visibleSize.height - 100));
    //    webView->loadURL("http://www.baidu.com");
    //    webView->setScalesPageToFit(true);
    //    this->addChild(webView);
    
    return true;
}

void HelloWorld::onEnter()
{
    Layer::onEnter();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    v_scene = dynamic_cast<Scene*>(this->getParent());
    
    auto sprite = Sprite::create("back.png");
    auto body = PhysicsBody::createCircle(sprite->getContentSize().width / 2.0, PhysicsMaterial(0.9f, 0.9f, 0.9f));
    sprite->setPhysicsBody(body);
    body->setTag(9);
    body->setAngularDamping(0.68);
    sprite->setPosition(visibleSize / 2.0);
    this->addChild(sprite);
    
    Node* node = Node::create();
    PhysicsBody* box = PhysicsBody::create(0.8,0.8);
    node->setPhysicsBody(box);
    box->setDynamic(false);
    node->setPosition(visibleSize / 2.0);
    this->addChild(node);
    
    v_scene->getPhysicsWorld()->addJoint(PhysicsJointPin::construct(sprite->getPhysicsBody(), box, sprite->getPosition()));
    
    Vec2 center_p = sprite->getContentSize() / 2.0;
    int  radius = 200;
    for (int i = 0; i < 12 ; i++){
        float sin_v = sin(CC_DEGREES_TO_RADIANS(i*-30));
        float cos_v = cos(CC_DEGREES_TO_RADIANS(i*-30));
        Vec2 spr_p = Vec2(radius * cos_v + center_p.x, radius * sin_v + center_p.y);
        
        Sprite *spr = Sprite::create("CloseNormal.png");
        auto body = PhysicsBody::createCircle(spr->getContentSize().width / 2.0);
        spr->setPhysicsBody(body);
        spr->setPosition(spr_p);
        sprite->addChild(spr);
        
//        Node* node = Node::create();
//        PhysicsBody* box = PhysicsBody::create(0.8,0.8);
//        node->setPhysicsBody(box);
//        box->setDynamic(false);
//        node->setPosition(spr_p);
//        sprite->addChild(node);
        
        Vec2 world_p = Vec2(spr->getPosition().x + sprite->getContentSize().width/2.0, spr->getPosition().y);
        v_scene->getPhysicsWorld()->addJoint(PhysicsJointPin::construct(spr->getPhysicsBody(), box, world_p));
    }
}

bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{
    auto location = touch->getLocation();
    auto arr = v_scene->getPhysicsWorld()->getShapes(location);
    
    PhysicsBody* body = nullptr;
    for (auto& obj : arr)
    {
        if (obj->getBody()->getTag() != 0)
        {
            body = obj->getBody();
            break;
        }
    }
    
    if (body != nullptr)
    {
        Node* mouse = Node::create();
        mouse->setPhysicsBody(PhysicsBody::create(PHYSICS_INFINITY, PHYSICS_INFINITY));
        mouse->getPhysicsBody()->setDynamic(false);
        mouse->setPosition(location);
        this->addChild(mouse);
        PhysicsJointPin* joint = PhysicsJointPin::construct(mouse->getPhysicsBody(), body, location);
        joint->setMaxForce(5000.0f * body->getMass());
        v_scene->getPhysicsWorld()->addJoint(joint);
        _mouses.insert(std::make_pair(touch->getID(), mouse));
        
        return true;
    }
    
    return false;
}

void HelloWorld::onTouchMoved(Touch* touch, Event* event)
{
    auto it = _mouses.find(touch->getID());
    
    if (it != _mouses.end())
    {
        it->second->setPosition(touch->getLocation());
    }
}

void HelloWorld::onTouchEnded(Touch* touch, Event* event)
{
    auto it = _mouses.find(touch->getID());
    
    if (it != _mouses.end())
    {
        this->removeChild(it->second);
        _mouses.erase(it);
    }
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    ModalLayer *ml = ModalLayer::create();
    ml->addLoading("back.png");
//    ml->addQuitConfirm("CloseNormal.png", "CloseSelected.png", [=](Ref* pSender){
//                            ml->removeFromParentAndCleanup(true);
//                       },
//                       "CloseNormal.png", "CloseSelected.png", [](Ref* pSender){
//                           Director::getInstance()->end();
//                           exit(0);
//                       },
//                       "HelloWorld.png");
    this->addChild(ml,10,10);
//    FacebookAgent::getInstance()->login([=](int ret, std::string& msg){
//        CCLOG("msg is %s", msg.c_str());
//    });
}
