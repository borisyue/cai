#include "CircleTurn.h"

#define PI 90.0
#define DAMPING 686

CircleTurn* CircleTurn::create(const std::string& filename)
{
    CircleTurn *sprite = new (std::nothrow) CircleTurn();
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->customInit();
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

// on "init" you need to initialize your instance
void CircleTurn::customInit()
{
    rotate_direction = Rotate_Direction_STOP;
    initOffsetAngle = 0;
    initAngle = 0;
    circle_center = this->getContentSize()/2.0;
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(CircleTurn::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(CircleTurn::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(CircleTurn::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(CircleTurn::onTouchCancelled, this);
    
    _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}

/*
 调整角度函数
 */
void adjustAngle(float *angle)
{
    // 调整角度
    if(*angle > 0 && *angle <= PI)
    {
        *angle = PI - *angle;
        return;
    }
    if( *angle > PI && *angle <= 2*PI )
    {
        *angle = 5 * PI - *angle;
        return;
    }
    if( *angle <= 0 )
    {
        *angle =  PI + fabsf(*angle);
        return;
    }
}

bool CircleTurn::onTouchBegan(Touch* touch, Event* event)
{
    if(rotate_direction != Rotate_Direction_STOP){ //非静止时表示正在旋转，屏蔽触摸事件
        return false;
    }

    Vec2 cPoint = this->convertTouchToNodeSpace(touch);

    if (cPoint.x < 0 || cPoint.x > getContentSize().width) {
        return false;
    }
    if (cPoint.y < 0 || cPoint.y > getContentSize().height) {
        return false;
    }
//    log("point: %f x %f", cPoint.x, cPoint.y);
    
    if (gettimeofday(&_lastTime, nullptr) != 0)
    {
        log("error in gettimeofday");
    }
    
    initAngle = (int)this->getRotation()%360;
    initOffsetAngle = ((cPoint - circle_center).getAngle());
    initOffsetAngle = CC_RADIANS_TO_DEGREES(initOffsetAngle);
    adjustAngle(&initOffsetAngle);//点击角度
    return true;
}

void CircleTurn::onTouchMoved(Touch* touch, Event* event)
{
    Vec2 cPoint = this->convertTouchToNodeSpace(touch);
    
    if (cPoint.x <= 0 || cPoint.x > getContentSize().width) {
        //进行结算处理
        switchCircleB();
        return;
    }
    if (cPoint.y <= 0 || cPoint.y > getContentSize().height) {
        //进行结算处理
        switchCircleB();
        return;
    }
    
    //根据起始触摸偏移角度，转盘起始角度，当前触摸点偏移角度，计算转盘的当前角度angle
    float newAngle = (cPoint - circle_center).getAngle();
    newAngle = CC_RADIANS_TO_DEGREES(newAngle);
    adjustAngle(&newAngle);
//    log("sprAngle:%f   targetAngle:%f", this->getRotation(), newAngle);
    switchCircleA(newAngle - initOffsetAngle);
}

void CircleTurn::onTouchEnded(Touch* touch, Event* event)
{
    switchCircleB();
}

void CircleTurn::onTouchCancelled(Touch *touch, Event *unused_event)
{
    switchCircleB();
}

void CircleTurn::switchCircleA(float angle)//转移角度
{
    float vAngle = int(angle + this->getRotation())%360;
    if (fabsf(angle) < 5)
    {
        return;
    }
    this->setRotation(vAngle);
}

void CircleTurn::switchCircleB()
{
    if(rotate_direction != Rotate_Direction_STOP){ //非静止时表示正在旋转，屏蔽触摸事件
        return;
    }
    
    struct timeval now;

    if (gettimeofday(&now, nullptr) != 0)
    {
        log("error in gettimeofday");
        return;
    }

    float _deltaTime = (now.tv_sec - _lastTime.tv_sec) + (now.tv_usec - _lastTime.tv_usec) / 1000000.0f;
    float _deltaAngle = this->getRotation() - initAngle;
    _deltaTime = MAX(0, _deltaTime);
    log("deltatime:%f, angle:%.1f - %.1f=%.1f",_deltaTime, this->getRotation(), initAngle , _deltaAngle);
    
    float speed = _deltaAngle/_deltaTime;
    if( _deltaTime > 1.5 || _deltaAngle < 19 || fabsf(speed) < 60 ) return; //速度慢 时间长 角度小 不考虑
    
    rotate_direction = (speed < 0) ? Rotate_Direction_CCW : Rotate_Direction_CW;
    float var_t = fabsf( speed / DAMPING );
    float total_angle = fabsf(speed / DAMPING) * speed;
    EaseOut *easeOut = EaseOut::create(RotateBy::create(var_t, total_angle), 9.8);
    log("var_t:%f, total_angle:%f", var_t, total_angle);
//    RotateBy *rotate = RotateBy::create(var_t, total_angle);
    CallFunc *callFunc = CallFunc::create([=](){
        rotate_direction = RotateDirection::Rotate_Direction_STOP;
    });
    Sequence *seq = Sequence::createWithTwoActions(easeOut, callFunc);
    this->runAction(seq);
}
