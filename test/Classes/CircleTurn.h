#ifndef __CircleTurn_H__
#define __CircleTurn_H__

#include "cocos2d.h"
USING_NS_CC;

typedef enum _RotateDirection
{
    Rotate_Direction_CW = 0x10,
    Rotate_Direction_STOP = 0x11,
    Rotate_Direction_CCW = 0x12,
}RotateDirection;

class CircleTurn : public cocos2d::Sprite
{
private:
    RotateDirection rotate_direction;
    struct timeval _lastTime;
    float initOffsetAngle; //初始化角度 for 触摸开始
    float initAngle;
    Vec2  circle_center; //圆心
public:

    virtual void customInit();
    
    void switchCircleB();
    void switchCircleA(float angle);

    bool onTouchBegan(Touch* touch, Event* event);
    void onTouchMoved(Touch* touch, Event* event);
    void onTouchEnded(Touch* touch, Event* event);
    void onTouchCancelled(Touch *touch, Event *unused_event);
    
    static CircleTurn* create(const std::string& filename);
};

#endif // __HELLOWORLD_SCENE_H__
